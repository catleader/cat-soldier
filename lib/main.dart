import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'server.dart';

void main() async {
  const env = String.fromEnvironment(
    'flavor',
    defaultValue: 'dev',
  );

  var endpoint = '';
  if (env == 'dev') {
    endpoint = devUrl;
  } else {
    endpoint = prodUrl;
  }

  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(
    const MyApp(
      env,
    ),
  );
}

class MyApp extends StatelessWidget {
  final String env;

  const MyApp(this.env, {super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(env),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage(this.env, {super.key});

  final String env;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var _appName = '';

  @override
  void initState() {
    super.initState();

    setState(() {
      _appName = Firebase.app().options.projectId;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: const Text('CatSolider'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Environment: ${widget.env}',
            ),
            Text(
              'App name: $_appName',
            ),
          ],
        ),
      ),
    );
  }
}
